import numpy as np

def read_train():
    return read("data/train.csv")

def read_test():
    return read("data/test.csv")

def read(fname):
    data = np.loadtxt(open(fname,"rb"),delimiter=",",skiprows=1,dtype=int)
    action = data[:,0]
    desc = data[:,1:]
    return action, desc
    

def write_predictions(ids,preds):
    f_out = open("predictions.csv","w")
    f_out.write("Id,Action\n")
    for i in xrange(len(ids)):
        f_out.write("%d,%f\n"%(ids[i],preds[i]))
    f_out.close()
