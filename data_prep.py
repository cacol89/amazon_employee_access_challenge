import numpy as np
from collections import defaultdict
from sklearn.preprocessing import OneHotEncoder

class DataMungler:

    def preprocess(self,x,y):
        m, n = x.shape

        self.comb_tup_map = {}
        pairs = [(a,b) for a in xrange(n) for b in xrange(a+1,n)]
        trips = [(a,b,c) for a in xrange(n) for b in xrange(a+1,n) for c in xrange(b+1,n)]
        self.combs = pairs+trips

        for cb in self.combs:
            self.comb_tup_map[cb] = self._encode_diff(x[:,list(cb)])

    def _encode_diff(self,a):
        n = 1 #0 is for None
        d = {}
        cnt = defaultdict(int)
        for row in a:
            t = tuple(row)
            cnt[t]+=1
        for row in a:
            t = tuple(row)
            if cnt[t] > 1 and t not in d:
                d[t] = n
                n += 1
        return d


    def extract_features(self,data):
        feats = [data]
        m,n = data.shape
        for cb in self.combs:
            d = self.comb_tup_map[cb]
            nf = np.zeros((m,1))
            sliced = data[:,list(cb)]
            for i in xrange(m):
                t = tuple(sliced[i,:])
                if t in d:
                    nf[i] = d[t]
            feats.append(nf)

        return np.hstack(tuple(feats))

def encode(data):
    return OneHotEncoder().fit_transform(data)
