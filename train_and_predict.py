import data_io,data_prep,argparse,my_stacker
from sklearn import (tree,pls,svm,linear_model,cross_validation,metrics,ensemble,naive_bayes,hmm,neighbors)
import numpy as np
from sklearn.preprocessing import OneHotEncoder

def seed():
    return 279745

def get_classifier():
    return my_stacker.MyStacker()
    #return linear_model.LogisticRegression(C=2,class_weight="auto")
    #return linear_model.SGDClassifier(loss='log',alpha=0.00005,n_iter=100)
    #return linear_model.SGDRegressor()
    #return naive_bayes.MultinomialNB(alpha=0.1)
    #return linear_model.Ridge(alpha=9)
    #return svm.SVR(kernel='rbf',C=3,gamma=0.5)
    #return linear_model.LinearRegression()
    #return neighbors.KNeighborsRegressor(weights='distance',n_neighbors=20)
    #return naive_bayes.BernoulliNB(alpha=0.05)
    #return linear_model.PassiveAggressiveRegressor(n_iter=1000,C=3)
    #return linear_model.Perceptron()

def cross_val(x,y,n):
    res = []
    cnt = 0
    #for train_idx, test_idx in cross_validation.StratifiedKFold(y, n_folds=n):
    sss = cross_validation.StratifiedShuffleSplit(y, n, test_size=1.0/n, random_state=seed())
    for train_idx, test_idx in sss:
        x_train, y_train = x[train_idx], y[train_idx]
        x_test, y_test = x[test_idx], y[test_idx]


        preds = train_and_predict(x_train,y_train,x_test,verbose=False)

        # compute AUC metric for this CV fold
        fpr, tpr, thresholds = metrics.roc_curve(y_test, preds)
        roc_auc = metrics.auc(fpr, tpr)
        res.append(roc_auc)
        cnt+=1
        print "...Finished fold %d/%d : %f"%(cnt,n,roc_auc)
    mean,std = np.mean(res),np.std(res)
    print "Cross validation results: Mean AUC = %f, stddev = %f" % (mean,std)
    return mean,std

def select_features(x,y,classifier):
    n_features = x.shape[1]
    maxi_mean, maxi_std, maxi_mask = 0,1.0,[]
    cnt = 0
    cv_folds = 5
    while True:
        cnt += 1
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",cnt,maxi_mask
        best_mean, best_std, best_opt = 0,1.0,None
        for i in xrange(n_features):
            if i in maxi_mask:
                continue
            mask = maxi_mask+[i]

            x_msk_enc = data_prep.encode(x[:,mask])

            (mean,std) = cross_val(classifier,x_msk_enc,y,cv_folds)

            if mean>best_mean or (mean==best_mean and std<best_std):
                best_mean, best_std, best_opt = mean,std,i
                
        if best_mean>maxi_mean or (best_mean==maxi_mean and best_std<maxi_std):
            maxi_mean, maxi_std = best_mean,best_std
            maxi_mask.append(best_opt)
            print "chose",best_opt
        else:
            break

    print "best mask: ",maxi_mask
    return maxi_mask

def train_and_predict(x_train,y_train,x_test,verbose=True):
    dm = data_prep.DataMungler()
    if verbose:
        print "Preprocessing data..."
    dm.preprocess(x_train,y_train)
    
    if verbose:
        print "Extracting features..."

    all_data = np.vstack((x_train,x_test))
    all_feats = dm.extract_features(np.vstack((x_train,x_test)))
   
    #Feature selection
    #mask = select_features(x_train,y_train,classifier)
    mask = [85, 0, 88, 45, 109, 58, 7, 82, 10, 52, 92, 119, 49, 47, 20, 26, 81, 65, 13, 122, 60] 
    all_feats = all_feats[:,mask]
    
    if verbose:
        print "Encoding features..."

    all_feats_enc = data_prep.encode(all_feats)
    
    m_train = x_train.shape[0]
    train_feats = all_feats_enc[:m_train,:]
    test_feats = all_feats_enc[m_train:,:]

    if verbose:
        print "Fitting classifier..."
    classifier = get_classifier()
    classifier.fit(train_feats,y_train)


    if verbose:
        print "Predicting..."
    if hasattr(classifier, 'predict_proba'):
        return classifier.predict_proba(test_feats)[:,1]
    return classifier.predict(test_feats)
    


def get_argparser():
    parser = argparse.ArgumentParser(description='Train and predict model for Amazon Employee Access challenge')
    parser.add_argument('-cv',dest='cv_folds', help='Number of cross validation folds.',default=0,type=int)
    parser.add_argument('-np', help='Don\'t make predictions',default=False,action='store_true')
    parser.add_argument('-o',dest='outfile', help='Output file where to store predictions',default="predictions.csv")
    return parser


def main():
    args = get_argparser().parse_args()

    print "Reading data..."
    y_train,x_train = data_io.read_train()
    ids_test,x_test = data_io.read_test()

    if args.cv_folds > 0:
        print "Cross validating..."     
        cross_val(x_train,y_train,args.cv_folds)
    
    if args.np:
        return

    preds = train_and_predict(x_train,y_train,x_test)

    print "Writting output..."
    data_io.write_predictions(ids_test,preds)

if __name__ == "__main__":
    main()
