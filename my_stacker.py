from sklearn import (tree,pls,svm,linear_model,cross_validation,metrics,ensemble,naive_bayes,hmm,neighbors)
import numpy as np

class MyStacker:
    def __init__(self):
        self.classifiers = [
            linear_model.SGDClassifier(loss='log',n_iter=100)
            #linear_model.SGDClassifier(loss='log',alpha=0.00005,class_weight="auto",n_iter=100)
            ,linear_model.LogisticRegression(C=2,class_weight="auto")
            #linear_model.LogisticRegression(C=3)
            #,svm.SVR(kernel='rbf',C=3,gamma=0.5)
            #,linear_model.Ridge(alpha=9)
            #,linear_model.LogisticRegression(C=2,class_weight="auto")
            #,linear_model.LinearRegression()
            #,svm.SVR(kernel='rbf')
            #,svm.NuSVR(kernel='rbf')
            #,linear_model.PassiveAggressiveRegressor(n_iter=20,C=5)
            #,linear_model.Perceptron()
            #,naive_bayes.BernoulliNB(alpha=0.05)
            #,naive_bayes.MultinomialNB(alpha=0.05)
        ]
        #self.stacker = ensemble.GradientBoostingClassifier(n_estimators=1000,learning_rate=0.1)
  
    def seed(self):
        return 2351237

    def best_preds(self,c,X):
        if hasattr(c, 'predict_proba'):
            return c.predict_proba(X)[:,1]
        else:
            return c.predict(X)

    def all_preds(self,X):
        preds = None

        n = len(self.classifiers)
        for i in xrange(n):
            y_test_preds = self.best_preds(self.classifiers[i],X)

            if preds == None:
                preds = y_test_preds
            else:
                preds = np.vstack((preds,y_test_preds))
            print "    > finished predicting %d/%d classifiers"%(i+1,n)

        return np.nan_to_num(np.transpose(preds))

    def fit_all(self,X,y):
        n = len(self.classifiers)
        for i in xrange(n):
            self.classifiers[i].fit(X,y)
            print "    > finished fitting %d/%d classifiers"%(i+1,n)


    def fit_twolevs(self,X,y):
        
        sss = cross_validation.StratifiedKFold(y, 5)
        
        X_stack, y_stack = None,None

        for train_idx, test_idx in sss:
            X_train, y_train = X[train_idx], y[train_idx]
            X_test, y_test = X[test_idx], y[test_idx]
            
            self.fit_all(X_train,y_train)
            preds = self.all_preds(X_test)

            if X_stack == None:
                X_stack, y_stack = preds, y_test
            else:
                X_stack = np.vstack((X_stack,preds))
                y_stack = np.hstack((y_stack,y_test))

        self.stacker.fit(X_stack,y_stack)
    
    def predict_twolevs(self,X):
        X_stack = self.all_preds(X)
        return self.best_preds(self.stacker,X_stack)
    
    def fit_mean(self,X,y):
        self.fit_all(X,y)

    def predict_mean(self,X):
        return np.mean(self.all_preds(X), axis=1)
        #preds = self.all_preds(X)
        #alpha = 0.20
        #return preds[:,0]*alpha+preds[:,1]*(1-alpha)
    
    def fit(self,X,y):
        return self.fit_mean(X,y)
        #return self.fit_twolevs(X,y)
    def predict(self,X):
        return self.predict_mean(X)
        #return self.predict_twolevs(X)
